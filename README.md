# Pecha Kucha

Pecha Kucha ist eine Präsentationsform, dessen Grundprinzip sich daran orientiert, bei einer Präsentation das Wesentliche im Blick zu halten, weswegen es nur 6:40min lang sein darf. 

Mit jedem Speichern (Commit) werden die folgenden Dokumente generiert

* [Kurs als Ebook](https://nettic.gitlab.io/pecha-kucha/pecha-kucha.epub)
* [Kurs als PDF](https://nettic.gitlab.io/pecha-kucha/pecha-kucha.pdf)
* [Kurs als HTML](https://nettic.gitlab.io/pecha-kucha/index.html)

Immer daran denken, folgende Änderungen zu machen:
* metadata.yml anpassen
    * manuell
    * mit [Metadaten-Generator](https://tibhannover.gitlab.io/oer/course-metadata-gitlab-form/metadata-generator.html) // UNDER CONSTRUCTION!
* pecha-kucha.md anpassen
* Links in der README.md anpassen
