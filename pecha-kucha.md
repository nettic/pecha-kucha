# Pecha Kucha

## Was ist Pecha Kucha?

Pecha Kucha ist eine Präsentationsform, dessen Grundprinzip sich daran
orientiert, bei einer Präsentation das Wesentliche im Blick zu halten. Es wurde 
im Jahr 2003 von den zwei Architekten Mark Dytham und Astrid Klein entwickelt. 
Pecha Kucha bedeutet aus dem Japanischen übersetzt „Schwatzen“ und meint, das 
Geräusch, dass entsteht, wenn viele Menschen gleichzeitig reden.

Der Hintergedanke dieser Präsentationsform ist, dass Vortragende oftmals dazu
tendieren, zu viel zu erzählen und auch die eigenen Präsentationsfolien bei 
PowerPoint zu voll zu machen. Die Informationsdichte auf den Folien und während 
des Vortrages ist oftmals zu hoch, sodass die Zuhörer und Zuschauer gar nicht
aufmerksam genug sein können („Death by PowerPoint“-Phänomen). Bei Pecha Kucha
Präsentationen muss darauf geachtet werden, dass die Informationen auf der Folie
in weniger als 20 Sekunden erfasst werden können. Daher wird meist auf Bilder
zurückgegriffen, anhand derer die Information vermittelt werden kann.

Die Vortragenden können ihre Ideen, Projekte und Geschichten im Schnelldurchgang
erzählen und müssen dabei die Zeit im Blick behalten. Der Vortrag wird 
unterstützt durch insgesamt 20 PowerPoint Folien, die jeweils nur 20 Sekunden 
gezeigt werden dürfen und automatisch weiterlaufen. Somit wird insgesamt nur 
6:40 Minuten vorgetragen. Der Vortragende muss sich genau überlegen, welche 
Informationen bei der jeweiligen Folie preisgibt, um die Zuschauer von seiner 
Idee oder seinem Produkt zu überzeugen.

## Pecha Kucha Nights

Mittlerweile finden in über 80 Städten weltweit Pecha Kucha Nights statt. Dabei 
sind Menschen und Vorträge aus allen Lebensbereichen willkommen. Die Methode hat
den Vorteil, dass an einem solchen Abend mehr Vortragende die Möglichkeit
bekommen, ihre Geschichte zu erzählen, weil alle Vortragenden maximal 6:40 
Minuten präsentieren. Zudem der Vorteil für die Zuhörer, denn auch sie können
mehreren Ideen zuhören, ohne, dass ihre Aufmerksamkeit schnell schwindet. Im 
Anschluss an den Vortrag kann man ggf. mit dem Vortragenden, oder der 
Vortragende mit interessierten Zuhörern ins Gespräch kommen und gemeinsam über 
das Thema diskutieren. So kann es doch schnell über 30 Minuten andauern, jedoch
ist diese Form der Auseinandersetzung mit einem Thema weitaus effektiver, sowohl
für den Vortragenden als auch für die Zuhörer.

## Pecha Kucha an der Schule & Universität

Das gleiche gilt für Schülerinnen und Schüler sowie Studierende. Auch eine 
Präsentation im Pecha Kucha Stil an der Schule oder Universität kann sehr 
ergiebig für den Unterricht oder ein Seminar sein. Sie lernen sich auf die 
wesentlichen Informationen zu beschränken und ihr Thema kurz und präzise
darzustellen. Es kann eine ausgiebige Diskussion im Anschluss folgen.

## Wie macht man einen Pecha Kucha Vortrag?

Um einen guten Pecha Kucha Vortrag vorzubereiten, sollten einige Schritte 
durchgeführt werden, die im Folgenden kurz erläutert werden. Dabei ist es 
besonders wichtig, nicht mit den Bildern und Folien zu beginnen, 
sondern inhaltlich:

1. Thema
In einem ersten Schritt muss das Thema des Vortrags festgelegt werden. Es sollte
sich dabei um ein Thema handeln, das Sie begeistert, welches Sie teilen wollen 
oder für das Sie noch Mitstreiter und weitere Ideen brauchen.

2. Ziel des Vortrags festlegen
Zu Beginn der Vorbereitung sollte zudem festgelegt werden, welches Ziel Sie 
verfolgen. Wollen Sie die Zuhörer informieren, inspirieren oder zu etwas bewegen
und ermuntern? Dieser Punkt sollte einem klar sein, damit man sich diesen in der
weiteren Planung immer wieder vor Augen halten kann.

3. Skript erstellen
Des Weiteren ist es hilfreich, sich vor dem Vortrag genau zu überlegen, was Sie
sagen möchten und dies in einer Art Skript niederzuschreiben. Damit können Sie
Ihren Vortrag gut strukturieren, die Folien auf den Inhalt abstimmen und üben, 
ob die 20 Sekunden pro Folie ausreichen.

Am meisten bleiben der Einstieg und das Ende als Erinnerung in den Köpfen der 
Zuhörer. Der Einstieg sollte motivierend und authentisch sein und die 
Aufmerksamkeit und das Interesse der Zuhörer wecken. Dies kann durch ein 
inspirierendes Bild, eine irritierende Aussage, eine direkte Frage oder ein 
kurzes Zitat gelingen. Zum Abschluss sollten Sie ihren Vortrag abrunden und die 
Kernbotschaft präzise zusammenfassen. Ein passendes Bild, das zu der Botschaft 
des Vortrags passt oder ein Handlungsaufruf kann ebenso eine besondere Wirkung 
zum Ende hin haben.

Wenn sie mit dem Hauptteil beginnen, sollten Sie nicht mehr als 3-5 Folien 
bereits genutzt haben. Um den Inhalt der nun folgenden Folien zu planen, 
empfiehlt es sich Unterpunkt in Form einer MindMap oder in einem Dokument zu 
sammeln. Diese sollten dann nach Relevanz geordnet und in eine Reihenfolge 
gebracht werden. Wenn das Skript in einer ersten Version fertig ist, lesen Sie 
es sich vor und stoppen die Zeit nach jedem Unterpunkt. Setzen Sie sich da alle 
20 Sekunden eine Markierung und passen Sie den Inhalt ggf. an.

4. Bilder

Wenn Ihr Skript fertig ist, können Sie die Folien erstellen und Bilder suchen. 
Bei der Suche der Bilder im Internet sollten Sie darauf achten, dass die 
lizenzfrei sind. Die Bilder sollten in 20 Sekunden erfasst werden können und 
inhaltlich das veranschaulichen, was Sie in den 20 Sekunden sagen möchten.

Ist der Vortrag und die Präsentation soweit fertig, gilt es nur noch zu üben, um
ein Gefühl dafür zu entwickeln, wie viel ich tatsächlich in den 6:40 Minuten 
sagen darf und sollte.

Ein Beispiel für diese Leitlinien finden Sie hier: _Was macht uns glücklich?_ [https://www.youtube.com/embed/w6fjUwWfZZg](https://www.youtube.com/embed/w6fjUwWfZZg)

### Beispiele

Erklärvideo zu „Was ist Pecha Kucha?“: [https://www.youtube.com/watch?v=8HvnesQxFBQ](https://www.youtube.com/watch?v=8HvnesQxFBQ)

